package dropi.dropiandroid;

import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONObject;

import java.util.List;

import model.ActivityStack;
import model.JSONParser;
import model.Utils;

/**
 * Created by user on 4/27/2016.
 */
public class SignUpActivity extends AppCompatActivity{
    EditText edt_uname,edt_pass,edt_confirm,edt_email,edt_name;
    RelativeLayout rl_next;
    Menu newmenu;
    boolean isNetwork;
    String uname, pass, pass2, email, name, status, response;
    ProgressBar progressBar;
    JSONParser parser;
    SharedPreferences prefs;
    JSONObject jobj;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        init();

        clickevents();

    }

    private void init() {

        parser = new JSONParser();
        prefs = getSharedPreferences("user_data", MODE_PRIVATE);

        toolbar = (Toolbar) findViewById(R.id.tool_bar); // Attaching the layout to the toolbar object
        TextView title=(TextView)findViewById(R.id.title);
        setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);// Setting toolbar as the ActionBar with setSupportActionBar() call
        Toolbar.LayoutParams params = new Toolbar.LayoutParams(Toolbar.LayoutParams.WRAP_CONTENT, Toolbar.LayoutParams.MATCH_PARENT);
        params.gravity = Gravity.CENTER_HORIZONTAL;
        title.setLayoutParams(params);
        title.setTypeface(Utils.setfontlight(SignUpActivity.this));
        title.setText(R.string.SignUp);

        edt_uname = (EditText)findViewById(R.id.edt_uname);
        edt_uname.setTypeface(Utils.setfontlight(SignUpActivity.this));
        edt_pass = (EditText)findViewById(R.id.edt_pass);
        edt_pass.setTypeface(Utils.setfontlight(SignUpActivity.this));
        edt_confirm = (EditText)findViewById(R.id.edt_confirm);
        edt_confirm.setTypeface(Utils.setfontlight(SignUpActivity.this));
        edt_email = (EditText)findViewById(R.id.edt_email);
        edt_email.setTypeface(Utils.setfontlight(SignUpActivity.this));
        edt_name = (EditText)findViewById(R.id.edt_name);
        edt_name.setTypeface(Utils.setfontlight(SignUpActivity.this));
        rl_next = (RelativeLayout)findViewById(R.id.rl_next);
        isNetwork = Utils.isNetworkConnected(SignUpActivity.this);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
    }

    private void clickevents() {

        rl_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_uname.length() == 0) {
                    Snackbar.make(edt_name, getResources().getString(R.string.uname_length), Snackbar.LENGTH_SHORT).show();
                } else if (edt_uname.length() < 4) {
                    Snackbar.make(edt_uname, getResources().getString(R.string.email_len), Snackbar.LENGTH_SHORT).show();
                } else if (edt_pass.length() == 0) {
                    Snackbar.make(edt_pass, getResources().getString(R.string.pass_length), Snackbar.LENGTH_SHORT).show();
                } else if (edt_pass.length() < 4) {
                    Snackbar.make(edt_pass, getResources().getString(R.string.pass_match_length), Snackbar.LENGTH_SHORT).show();
                } else if (edt_confirm.length() == 0) {
                    Snackbar.make(edt_confirm,getResources().getString(R.string.pass_length), Snackbar.LENGTH_SHORT).show();
                } else if (!edt_pass.getText().toString().equals(edt_confirm.getText().toString())) {
                    Snackbar.make(edt_confirm, getResources().getString(R.string.pass_match), Snackbar.LENGTH_SHORT).show();
                } else if (edt_email.length() == 0) {
                    Snackbar.make(edt_email,getResources().getString(R.string.email_length), Snackbar.LENGTH_SHORT).show();
                } else if (edt_name.length() == 0) {
                    Snackbar.make(edt_name, getResources().getString(R.string.name_length), Snackbar.LENGTH_SHORT).show();
                } else {
                    if (!isNetwork) {
                        Utils.conDialog(SignUpActivity.this);
                    } else {
                        Utils.hidekeyboard(SignUpActivity.this);
                        uname = edt_uname.getText().toString();
                        pass = edt_pass.getText().toString();
                        pass2 = edt_confirm.getText().toString();
                        email = edt_email.getText().toString();
                        name = edt_name.getText().toString();
                        new sign_up().execute();
                    }
                }
            }
        });

    }

    //*******************************onCreateOptionsMenu********************************************
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.newmenu = menu;
        getMenuInflater().inflate(R.menu.menu_main, newmenu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                return true;
        }
        return false;
    }

    //*******************************************Login Async Class**********************************
    class sign_up extends AsyncTask<Void, Void, Void> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
//            if (MainActivity.profile != null) {
//                response = sign_up_fb(uname, pass, pass2, email, name, String.valueOf(MainActivity.lat),
//                        String.valueOf(MainActivity.lng), MainActivity.city, MainActivity.profile);
//            } else {
                response = sign_up(uname, pass, pass2, email, name, String.valueOf(Utils.lat),
                        String.valueOf(Utils.lng), Utils.city);
//            }
            Log.i("Response", ":" + response);
            try {
                jobj = new JSONObject(response);
                if (jobj.has("success")) {
                    Utils.token = jobj.optString("success");
                    Log.i("Status", ":" + Utils.token);
                    //Add Values to prefs
                    SharedPreferences.Editor edit = prefs.edit();
                    edit.putString("key", Utils.token);
                    edit.putString("uname", uname);
                    edit.commit();
                } else {
                    status = jobj.optString("error");
                    Log.i("Status", ":" + status);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressBar.setVisibility(View.GONE);
            if (jobj.has("success")) {
                startActivity(new Intent(SignUpActivity.this, DashboardActivity.class));
                ActivityStack.activity.add(SignUpActivity.this);
                finish();
            } else {
                Utils.dialog(SignUpActivity.this, "Alert!", status);
            }
        }
    }


    //*******************************************Login Method***************************************
    public String sign_up(String username, String password, String password2, String email, String name, String longitude
            , String latitude, String location) {
        // TODO Auto-generated method stub
        Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("email", email)
                .appendQueryParameter("latitude", latitude)
                .appendQueryParameter("location", location)
                .appendQueryParameter("longitude", longitude)
                .appendQueryParameter("name", name)
                .appendQueryParameter("password", password)
                .appendQueryParameter("password2", password2)
                .appendQueryParameter("username", username);
        Log.i("Complete", "Url: " + Utils.sign_up + builder);
        String response = null;
        response = parser.getJSONFromUrl(Utils.sign_up, builder, "");
        return response;
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
