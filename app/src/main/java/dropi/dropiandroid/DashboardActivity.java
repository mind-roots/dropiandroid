package dropi.dropiandroid;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.widget.TextView;

import fragments.ActivityFragment;
import fragments.DropFragment;
import fragments.FeedFragment;
import fragments.ProfileFragment;
import fragments.SettingsFragment;
import model.ActivityStack;
import model.JSONParser;
import model.Utils;

/**
 * Created by user on 4/27/2016.
 */
public class DashboardActivity extends AppCompatActivity{
    Menu newmenu;
    TabLayout tabLayout;
    public static TextView title;
    private Toolbar toolbar;
    Fragment fragment;
    public static TabLayout.Tab tab4;
    JSONParser parser;
    SharedPreferences prefs;

    public  static FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        init();

        tabs();

      tabLayout.getTabAt(2).setCustomView(R.layout.custom_tab);

       // tabLayout.getTabAt(3).setCustomView(TabUtils.renderTabView(DashboardActivity.this, R.drawable.ic_intr, 0, Utils.unread_count));

        clickevents();

    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void tabs() {
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
       tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.ic_feed));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.ic_activity));
        tabLayout.addTab(tabLayout.newTab());
        //  tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.ic_intr));
        tab4 = tabLayout.newTab();

        tabLayout.addTab(tab4.setIcon(R.drawable.ic_profile));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.ic_settings));
        tabLayout.setBackground(new ColorDrawable(Color.parseColor("#FFFFFF")));



        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setTabMode(TabLayout.MODE_FIXED);
    }

    private void init() {
        toolbar = (Toolbar) findViewById(R.id.tool_bar); // Attaching the layout to the toolbar object
        toolbar.setNavigationIcon(R.drawable.ic_explore);
        title = (TextView) findViewById(R.id.title);
        setTitle("");
        setSupportActionBar(toolbar);
        Toolbar.LayoutParams params = new Toolbar.LayoutParams(Toolbar.LayoutParams.WRAP_CONTENT, Toolbar.LayoutParams.MATCH_PARENT);
        params.gravity = Gravity.CENTER_HORIZONTAL;
        title.setLayoutParams(params);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);// Setting toolbar as the ActionBar with setSupportActionBar() call
        title.setTypeface(Utils.setfontlight(DashboardActivity.this));
        title.setText(R.string.feed);

        fragmentManager = getFragmentManager();

        prefs = getSharedPreferences("user_data", MODE_PRIVATE);
        fragment = new FeedFragment();
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
        parser = new JSONParser();
    }

    private void clickevents() {
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {
                    toolbar.setNavigationIcon(R.drawable.ic_explore);
                    title.setTypeface(Utils.setfontlight(DashboardActivity.this));
                    title.setTextSize(22);
                    title.setText(R.string.feed);
                    newmenu.findItem(R.id.action_msg).setVisible(true);
                    fragment = new FeedFragment();
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
                } else if (tab.getPosition() == 1) {
                    toolbar.setNavigationIcon(null);
                    title.setTypeface(Utils.setfontlight(DashboardActivity.this));
                    title.setText(R.string.activity);
                    title.setTextSize(22);
                    newmenu.findItem(R.id.action_msg).setVisible(false);
                    fragment = new ActivityFragment();
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
                } else if (tab.getPosition() == 2) {
                    toolbar.setNavigationIcon(null);
                    title.setTypeface(Utils.setfontlight(DashboardActivity.this));
                    title.setText(R.string.app_name);
                    title.setTextSize(22);
                    newmenu.findItem(R.id.action_msg).setVisible(false);
                    fragment = new DropFragment();
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
                } else if (tab.getPosition() == 3) {
                    toolbar.setNavigationIcon(null);
                    title.setTypeface(Utils.setfontlight(DashboardActivity.this));
                    title.setText(R.string.profile);
                    title.setTextSize(22);
                    newmenu.findItem(R.id.action_msg).setVisible(false);
                    fragment = new ProfileFragment();
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
                } else if (tab.getPosition() == 4) {
                    toolbar.setNavigationIcon(null);
                    title.setTypeface(Utils.setfontlight(DashboardActivity.this));
                    title.setText(R.string.settings);
                    title.setTextSize(22);
                    newmenu.findItem(R.id.action_msg).setVisible(false);
                    fragment = new SettingsFragment();
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }


    //*******************************onCreateOptionsMenu********************************************
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.newmenu = menu;
        getMenuInflater().inflate(R.menu.menu_feed, newmenu);
//        newmenu.findItem(R.id.action_settings).setVisible(false);
//        newmenu.findItem(R.id.action_next).setVisible(false);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    //*************************************Back Press Method****************************************
    @Override
    public void onBackPressed() {
        exitdialog();
    }

    //**********************Back Dialog********************
    private void exitdialog() {


        AlertDialog.Builder alert = new AlertDialog.Builder(DashboardActivity.this);
        alert.setTitle("Alert!");
        alert.setMessage("Are you sure you want to exit from this app?");
        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {


                for (int i = 0; i < ActivityStack.activity
                        .size(); i++) {
                    (ActivityStack.activity.elementAt(i))
                            .finish();
                    Log.e("Activity size:" + ActivityStack.activity.size(), "finished...");
                }
                System.exit(0);
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
            }
        });

        alert.show();

    }



}
