package dropi.dropiandroid;

import android.app.ActionBar;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONObject;

import model.ActivityStack;
import model.JSONParser;
import model.Utils;

/**
 * Created by user on 4/27/2016.
 */
public class LogInActivity extends AppCompatActivity {
    EditText edt_uname, edt_pass;
    TextView txt_forgot,txt_term,txt_policy;
    RelativeLayout rl_next;
    Menu newmenu;
    private Toolbar toolbar;
    boolean isNetwork;
    String uname, pass, status;
    ProgressBar progressBar;
    SharedPreferences prefs;
    JSONParser parser;
    JSONObject jobj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        init();

        clickevents();

    }

    private void init() {

        parser = new JSONParser();
        prefs = getSharedPreferences("user_data", MODE_PRIVATE);

        toolbar = (Toolbar) findViewById(R.id.tool_bar); // Attaching the layout to the toolbar object
        TextView title=(TextView)findViewById(R.id.title);
        setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);// Setting toolbar as the ActionBar with setSupportActionBar() call
        Toolbar.LayoutParams params = new Toolbar.LayoutParams(Toolbar.LayoutParams.WRAP_CONTENT, Toolbar.LayoutParams.MATCH_PARENT);
        params.gravity = Gravity.CENTER_HORIZONTAL;
        title.setLayoutParams(params);
        title.setTypeface(Utils.setfontlight(LogInActivity.this));
        title.setText(R.string.LogIn);

        edt_uname = (EditText) findViewById(R.id.edt_uname);
        edt_uname.setTypeface(Utils.setfontlight(LogInActivity.this));
        edt_pass = (EditText) findViewById(R.id.edt_pass);
        edt_pass.setTypeface(Utils.setfontlight(LogInActivity.this));
        txt_forgot = (TextView) findViewById(R.id.txt_forgot);
        txt_forgot.setText(Html.fromHtml(getString(R.string.forgot)));
        txt_forgot.setTypeface(Utils.setfontlight(LogInActivity.this));
        txt_term = (TextView) findViewById(R.id.txt_term);
        txt_term.setText(Html.fromHtml(getString(R.string.txt_term)));
        txt_term.setTypeface(Utils.setfontlight(LogInActivity.this));
        txt_policy = (TextView) findViewById(R.id.txt_policy);
        txt_policy.setText(Html.fromHtml(getString(R.string.txt_policy)));
        txt_policy.setTypeface(Utils.setfontlight(LogInActivity.this));
        rl_next = (RelativeLayout) findViewById(R.id.rl_next);
        isNetwork = Utils.isNetworkConnected(LogInActivity.this);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
    }

    private void clickevents() {

        rl_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_uname.length() == 0) {
                    Snackbar.make(edt_uname, getResources().getString(R.string.uname_length), Snackbar.LENGTH_SHORT).show();
                } else if (edt_pass.length() == 0) {
                    Snackbar.make(edt_pass, getResources().getString(R.string.pass_length), Snackbar.LENGTH_SHORT).show();
                } else {
                    if (!isNetwork) {
                        Utils.conDialog(LogInActivity.this);
                    } else {
                        uname = edt_uname.getText().toString();
                        pass = edt_pass.getText().toString();
                        Utils.hidekeyboard(LogInActivity.this);
                        new login().execute();
                    }

                }
            }
        });

    }

    //*******************************onCreateOptionsMenu********************************************
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.newmenu = menu;
        getMenuInflater().inflate(R.menu.menu_main, newmenu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                return true;
        }
        return false;
    }

    //*******************************************Login Async Class**********************************
    class login extends AsyncTask<Void, Void, Void> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            String response = login(uname, pass);
            Log.i("Response", ":" + response);
            try {
                jobj = new JSONObject(response);
                if (jobj.has("success")) {
                    status = jobj.optString("success");
                    Log.i("Status", ":" + status);
                    //Add Values to prefs
                    SharedPreferences.Editor edit = prefs.edit();
                    edit.putString("key", status);
                    edit.putString("uname", uname);
                    edit.commit();
                } else {
                    status = jobj.optString("error");
                    Log.i("Status", ":" + status);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressBar.setVisibility(View.GONE);
            if(jobj.has("success")){
                startActivity(new Intent(LogInActivity.this, DashboardActivity.class));
                ActivityStack.activity.add(LogInActivity.this);
            }else {
                Utils.dialog(LogInActivity.this, "Alert!", status);
            }
        }
    }


    //*******************************************Login Method***************************************
    public String login(String username, String password) {
        // TODO Auto-generated method stub
        Uri.Builder builder = new Uri.Builder().appendQueryParameter("username", username)
                .appendQueryParameter(
                        "password", password);
        Log.i("Complete", "Url: " + Utils.log_in + builder);
        String response = null;
        response = parser.getJSONFromUrl(Utils.log_in, builder, prefs.getString("key",null));
        return response;
    }

    @Override
    public void onBackPressed() {
        finish();
    }

}
