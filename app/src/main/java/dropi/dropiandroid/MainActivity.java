package dropi.dropiandroid;

import android.app.Activity;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.Locale;

import model.ActivityStack;
import model.GpsTracker;
import model.Utils;

public class MainActivity extends Activity {
    ImageView img_fwd;
    Menu newmenu;
    TextView txt_signup,txt_title;
    GpsTracker gps;
    List<Address> addresses;
    Geocoder geocoder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();

        clickevents();

    }

    //*******************************UI Initialization********************************************
    private void init() {
        img_fwd = (ImageView) findViewById(R.id.img_fwd);
        txt_signup = (TextView) findViewById(R.id.txt_signup);
        txt_title = (TextView) findViewById(R.id.txt_title);
        txt_signup.setTypeface(Utils.setfontlight(MainActivity.this));
        txt_title.setTypeface(Utils.setfontlight(MainActivity.this));
        setTitle("");

        gps = new GpsTracker(MainActivity.this);
        Utils.lat = gps.latitude;
        Utils.lng = gps.longitude;
        Log.i("Location",""+Utils.lat+""+Utils.lng);
        try {
            geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
            addresses = geocoder.getFromLocation(Utils.lat, Utils.lng, 1);
            Utils.city = addresses.get(0).getLocality();
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Unable to get location",
                    Toast.LENGTH_SHORT).show();
        }
    }

    //*******************************On Click Methods********************************************
    private void clickevents() {
        img_fwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, LogInActivity.class));
                ActivityStack.activity.add(MainActivity.this);
            }
        });

        txt_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, SignUpActivity.class));
                ActivityStack.activity.add(MainActivity.this);
            }
        });

    }

    //*******************************onCreateOptionsMenu********************************************
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.newmenu = menu;
        getMenuInflater().inflate(R.menu.menu_signup, newmenu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                return true;
        }
        if (item.getItemId() == R.id.action_sign) {

        }
        return false;
    }

}
