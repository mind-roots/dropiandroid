package dropi.dropiandroid;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import model.ActivityStack;

/**
 * Created by user on 4/27/2016.
 */
public class SplashActivity extends Activity {
    private static int SPLASH_TIME_OUT = 3000;
    SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        prefs = getSharedPreferences("user_data", MODE_PRIVATE);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                if (prefs.getString("key", null) != null) {
                    startActivity(new Intent(SplashActivity.this, DashboardActivity.class));
                    ActivityStack.activity.add(SplashActivity.this);
                    finish();
                } else {
                    startActivity(new Intent(SplashActivity.this, MainActivity.class));
                    ActivityStack.activity.add(SplashActivity.this);
                    finish();
                }
            }
        }, SPLASH_TIME_OUT);

    }
}
