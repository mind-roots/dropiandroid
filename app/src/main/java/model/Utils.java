package model;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.provider.Settings;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

/**
 * Created by user on 4/27/2016.
 */
public class Utils {
    public static String base_url = "http://dropiapp.herokuapp.com/api/v1/";
    public static String sign_up = base_url + "accounts/register";
    public static String log_in = base_url + "accounts/login";
    public static String upload_posts = base_url + "";
    public static String edit_post = base_url + "";
    public static String token = "";
    public static String city;
    public static double lat, lng;

    // **************************************Connection check**************************************
    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    // **************************************Connection Dialog**************************************
    public static void conDialog(final Context c) {
        AlertDialog.Builder alert = new AlertDialog.Builder(
                c);

        alert.setTitle("Internet connection unavailable.");
        alert.setMessage("You must be connected to an internet connection via WI-FI or Mobile Connection.");
        alert.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int whichButton) {
                        c.startActivity(new Intent(
                                Settings.ACTION_WIRELESS_SETTINGS));
                    }
                });

        alert.show();
    }

    // *******************************************Alert Dialog**************************************
    public static void dialog(Context ctx, String title, String msg) {

        new AlertDialog.Builder(ctx)
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // continue with delete
                                dialog.dismiss();
                            }
                        })

                .setIcon(android.R.drawable.ic_dialog_alert).show();
    }

    public static void hidekeyboard(Context ctx) {
        // TODO Auto-generated method stub
        View view = ((Activity) ctx).getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) ((Activity) ctx).getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }


    //************************************Setting Font in application*******************************
    public static Typeface setfontlight(Context c) {
        Typeface font = Typeface.createFromAsset(c.getAssets(), "fonts/OpenSans-Regular.ttf");
        return font;
    }

}
