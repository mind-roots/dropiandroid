package model;

import android.net.Uri;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public class JSONParser {
    // constructor
    public JSONParser() {

    }

    /*             key value pairs
                builder = new Uri.Builder()
                        .appendQueryParameter("auth_code", auth_code);*/
    public String getJSONFromUrl(String urlAddress, Uri.Builder builder, String header) {
        StringBuilder tempData = new StringBuilder();
        InputStream in = null;
        try {
            URL url = new URL(urlAddress);
            HttpURLConnection con = (HttpURLConnection) url
                    .openConnection();
            con.setDoOutput(true);
            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            // add request header
            con.setRequestMethod("POST");

            // Send post request, compulsory when sending post request
            con.setDoOutput(true);

            String query = builder.build().getEncodedQuery();

            //adds key value pair to your request
            OutputStream os = con.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(query);
            writer.flush();
            writer.close();
            os.close();

            // use this response code to get status codes like 200 is for
            // success,500 for error on server side etc
            int responseCode = con.getResponseCode();
            System.out.println(" code hh : " + responseCode);
            in = con.getInputStream();
            // in = url.openStream(); remove this line
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(in));
            String line;
            while ((line = reader.readLine()) != null) {
                tempData.append(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (in != null)
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }

        return tempData.toString();
    }

    public String getJSON(String urlAddress, String feild, String value, String page,String header) {
        String charset = "UTF-8";
        StringBuilder tempData = new StringBuilder();
        InputStream in = null;
        try {
            MultipartUtility multipart = null;
            try {
                multipart = new MultipartUtility(urlAddress, charset, header);
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (value != "") {
                System.out.println("category=" + value);
                multipart.addFormField(feild, value);
            }
            // multipart.addHeaderField("Priya", "veAwLKDAHodwmtn7zBagYWcSmfBnsqeYEPupy8ZgxdOTGb1gNcyL9LDw1eTuE8sj");
            multipart.addFormField("page", page);
            List<String> response = multipart.finish();
            //  "Token token=\"N9EJjmvexbGncgEkkBS14OxS5FQxy3Akcf4wBAM8NswZJbj4Ob5SZDL4iBBSEcdS\"";
            // [self setDefaultHeader:@"Priya" value:[NSString stringWithFormat:@"Token token=\"%@\"", token]];
            String res = null;
            for (String line : response) {
                System.out.println(line);
                res = line;
            }
            return res;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (in != null)
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }

        return null;
    }

    public String getAuth(String urlAddress, String device_id, String type, String header) {
        String charset = "UTF-8";
        StringBuilder tempData = new StringBuilder();
        InputStream in = null;
        try {
            MultipartUtility multipart = null;
            try {
                multipart = new MultipartUtility(urlAddress, charset, header);
            } catch (IOException e) {
                e.printStackTrace();
            }

            multipart.addFormField("device_id", device_id);
            // multipart.addHeaderField("Priya", "veAwLKDAHodwmtn7zBagYWcSmfBnsqeYEPupy8ZgxdOTGb1gNcyL9LDw1eTuE8sj");
            multipart.addFormField("type", type);
            List<String> response = multipart.finish();
            //  "Token token=\"N9EJjmvexbGncgEkkBS14OxS5FQxy3Akcf4wBAM8NswZJbj4Ob5SZDL4iBBSEcdS\"";
            // [self setDefaultHeader:@"Priya" value:[NSString stringWithFormat:@"Token token=\"%@\"", token]];
            String res = null;
            for (String line : response) {
                System.out.println(line);
                res = line;
            }
            return res;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (in != null)
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }

        return null;
    }

    public String postJSON(String urlAddress, String feild, String value,String feild2, String value2) {
        String charset = "UTF-8";
        StringBuilder tempData = new StringBuilder();
        InputStream in = null;
        try {
            MultipartUtility multipart = null;
            try {
                multipart = new MultipartUtility(urlAddress, charset, "");
            } catch (IOException e) {
                e.printStackTrace();
            }

            if(feild.equals("")) {

            }else {
                multipart.addFormField(feild, value);
            }
            // multipart.addHeaderField("Priya", "veAwLKDAHodwmtn7zBagYWcSmfBnsqeYEPupy8ZgxdOTGb1gNcyL9LDw1eTuE8sj");
            multipart.addFormField(feild2, value2);
         //   multipart.addFormField("post", id);
            List<String> response = multipart.finish();
            //  "Token token=\"N9EJjmvexbGncgEkkBS14OxS5FQxy3Akcf4wBAM8NswZJbj4Ob5SZDL4iBBSEcdS\"";
            // [self setDefaultHeader:@"Priya" value:[NSString stringWithFormat:@"Token token=\"%@\"", token]];
            String res = null;
            for (String line : response) {
                System.out.println(line);
                res = line;
            }
            return res;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (in != null)
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }

        return null;
    }

    public String postReport(String urlAddress, String id) {
        String charset = "UTF-8";
        StringBuilder tempData = new StringBuilder();
        InputStream in = null;
        try {
            MultipartUtility multipart = null;
            try {
                multipart = new MultipartUtility(urlAddress, charset, "");
            } catch (IOException e) {
                e.printStackTrace();
            }
            multipart.addFormField("id", id);
            List<String> response = multipart.finish();
            //  "Token token=\"N9EJjmvexbGncgEkkBS14OxS5FQxy3Akcf4wBAM8NswZJbj4Ob5SZDL4iBBSEcdS\"";
            // [self setDefaultHeader:@"Priya" value:[NSString stringWithFormat:@"Token token=\"%@\"", token]];
            String res = null;
            for (String line : response) {
                System.out.println(line);
                res = line;
            }
            return res;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (in != null)
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }

        return null;
    }

    public String getJSONdata(String urlAddress, String header) {
        String charset = "UTF-8";
        StringBuilder tempData = new StringBuilder();
        InputStream in = null;
        try {
            MultipartUtility multipart = null;
            try {
                multipart = new MultipartUtility(urlAddress, charset, header);
            } catch (IOException e) {
                e.printStackTrace();
            }


            //  multipart.addHeaderField("Priya", "veAwLKDAHodwmtn7zBagYWcSmfBnsqeYEPupy8ZgxdOTGb1gNcyL9LDw1eTuE8sj");

            List<String> response = multipart.finish();
            //  "Token token=\"N9EJjmvexbGncgEkkBS14OxS5FQxy3Akcf4wBAM8NswZJbj4Ob5SZDL4iBBSEcdS\"";
            // [self setDefaultHeader:@"Priya" value:[NSString stringWithFormat:@"Token token=\"%@\"", token]];
            String res = null;
            for (String line : response) {
                System.out.println(line);
                res = line;
            }
            return res;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (in != null)
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }

        return null;
    }

    public String upload_post(String image, String name, String category, String caption) {

        String charset = "UTF-8";
        File sourceFile = new File(image);
        String requestURL = Utils.upload_posts;
        try {
            MultipartUtility multipart = null;
            try {
                multipart = new MultipartUtility(requestURL, charset, "");
            } catch (IOException e) {
                e.printStackTrace();
            }
            multipart.addFormField("name", name);
            multipart.addFormField("type", "image");
            multipart.addFormField("category", category);
            multipart.addFormField("caption", caption);
            multipart.addFilePart("Image", sourceFile);
            List<String> response = multipart.finish();

            String res = null;
            for (String line : response) {
                System.out.println(line);
                res = line;
            }
            return res;
        } catch (IOException ex) {
            System.err.println(ex);
        }

        return null;
    }

    public String edit_post(String type, String image, String name, String id, String caption) {

        String charset = "UTF-8";
        File sourceFile = new File(image);
        String requestURL = Utils.edit_post;
        try {
            MultipartUtility multipart = null;
            try {
                multipart = new MultipartUtility(requestURL, charset, "");
            } catch (IOException e) {
                e.printStackTrace();
            }
            multipart.addFormField("name", name);
            multipart.addFormField("id", id);
            multipart.addFormField("caption", caption);
            if (type.equals("path")) {
            } else {
                multipart.addFilePart("Image", sourceFile);
            }
            List<String> response = multipart.finish();

            String res = null;
            for (String line : response) {
                System.out.println(line);
                res = line;
            }
            return res;
        } catch (IOException ex) {
            System.err.println(ex);
        }

        return null;
    }

    public String getUpdateField(String urlAddress, String field, String value, String header) {
        String charset = "UTF-8";
        StringBuilder tempData = new StringBuilder();
        InputStream in = null;
        try {
            MultipartUtility multipart = null;
            try {
                multipart = new MultipartUtility(urlAddress, charset, header);
            } catch (IOException e) {
                e.printStackTrace();
            }


            multipart.addFormField("field", field);
            multipart.addFormField("value", value);
            List<String> response = multipart.finish();
            //  "Token token=\"N9EJjmvexbGncgEkkBS14OxS5FQxy3Akcf4wBAM8NswZJbj4Ob5SZDL4iBBSEcdS\"";
            // [self setDefaultHeader:@"Priya" value:[NSString stringWithFormat:@"Token token=\"%@\"", token]];
            String res = null;
            for (String line : response) {
                System.out.println(line);
                res = line;
            }
            return res;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (in != null)
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }

        return null;
    }


    public String changeName(String urlAddress, String field, String id) {
        String charset = "UTF-8";
        StringBuilder tempData = new StringBuilder();
        InputStream in = null;
        try {
            MultipartUtility multipart = null;
            try {
                multipart = new MultipartUtility(urlAddress, charset, "");
            } catch (IOException e) {
                e.printStackTrace();
            }

            multipart.addFormField("field", field);
            multipart.addFormField("value", id);
            List<String> response = multipart.finish();
            //  "Token token=\"N9EJjmvexbGncgEkkBS14OxS5FQxy3Akcf4wBAM8NswZJbj4Ob5SZDL4iBBSEcdS\"";
            // [self setDefaultHeader:@"Priya" value:[NSString stringWithFormat:@"Token token=\"%@\"", token]];
            String res = null;
            for (String line : response) {
                System.out.println(line);
                res = line;
            }
            return res;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (in != null)
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }

        return null;
    }

    public String forgot_pass(String urlAddress, String field) {
        String charset = "UTF-8";
        StringBuilder tempData = new StringBuilder();
        InputStream in = null;
        try {
            MultipartUtility multipart = null;
            try {
                multipart = new MultipartUtility(urlAddress, charset, "");
            } catch (IOException e) {
                e.printStackTrace();
            }

            multipart.addFormField("email", field);
            List<String> response = multipart.finish();
            //  "Token token=\"N9EJjmvexbGncgEkkBS14OxS5FQxy3Akcf4wBAM8NswZJbj4Ob5SZDL4iBBSEcdS\"";
            // [self setDefaultHeader:@"Priya" value:[NSString stringWithFormat:@"Token token=\"%@\"", token]];
            String res = null;
            for (String line : response) {
                System.out.println(line);
                res = line;
            }
            return res;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (in != null)
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }

        return null;
    }


    public String changeLocation(String urlAddress, String latitude, String longitude, String Location) {
        String charset = "UTF-8";
        StringBuilder tempData = new StringBuilder();
        InputStream in = null;
        try {
            MultipartUtility multipart = null;
            try {
                multipart = new MultipartUtility(urlAddress, charset, "");
            } catch (IOException e) {
                e.printStackTrace();
            }

            multipart.addFormField("latitude", latitude);
            multipart.addFormField("longitude", longitude);
            multipart.addFormField("location", Location);
            List<String> response = multipart.finish();
            //  "Token token=\"N9EJjmvexbGncgEkkBS14OxS5FQxy3Akcf4wBAM8NswZJbj4Ob5SZDL4iBBSEcdS\"";
            // [self setDefaultHeader:@"Priya" value:[NSString stringWithFormat:@"Token token=\"%@\"", token]];
            String res = null;
            for (String line : response) {
                System.out.println(line);
                res = line;
            }
            return res;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (in != null)
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }

        return null;
    }

    public String changeAvatar(String url,String image ) {

        String charset = "UTF-8";
        File sourceFile = new File(image);
        try {
            MultipartUtility multipart = null;
            try {
                multipart = new MultipartUtility(url, charset, "");
            } catch (IOException e) {
                e.printStackTrace();
            }
            multipart.addFilePart("Image", sourceFile);
            List<String> response = multipart.finish();

            String res = null;
            for (String line : response) {
                System.out.println(line);
                res = line;
            }
            return res;
        } catch (IOException ex) {
            System.err.println(ex);
        }

        return null;
    }


    public String logout(String urlAddress, String push_token, String authcode) {
        String charset = "UTF-8";
        StringBuilder tempData = new StringBuilder();
        InputStream in = null;
        try {
            MultipartUtility multipart = null;
            try {
                multipart = new MultipartUtility(urlAddress, charset, "");
            } catch (IOException e) {
                e.printStackTrace();
            }

          //  multipart.addFormField("push_token", push_token);
            multipart.addFormField("authcode", authcode);
            List<String> response = multipart.finish();
            //  "Token token=\"N9EJjmvexbGncgEkkBS14OxS5FQxy3Akcf4wBAM8NswZJbj4Ob5SZDL4iBBSEcdS\"";
            // [self setDefaultHeader:@"Priya" value:[NSString stringWithFormat:@"Token token=\"%@\"", token]];
            String res = null;
            for (String line : response) {
                System.out.println(line);
                res = line;
            }
            return res;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (in != null)
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }

        return null;
    }

}