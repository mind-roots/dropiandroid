package fragments;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;
import java.util.Locale;

import dropi.dropiandroid.DashboardActivity;
import dropi.dropiandroid.MainActivity;
import dropi.dropiandroid.R;
import model.GpsTracker;
import model.Utils;

/**
 * Created by user on 4/27/2016.
 */
public class FeedFragment extends Fragment {
    View rootView;
    GpsTracker gps;
    FrameLayout layout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_feed, container, false);

        init();

        return rootView;
    }

    private void init() {

        layout = (FrameLayout)rootView.findViewById(R.id.map);

        gps = new GpsTracker(getActivity());

        MapFragment mapFragment =  MiniMapFragment.newInstance(new LatLng(gps.latitude, gps.longitude));
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(R.id.map, mapFragment).commit();

    }

}

